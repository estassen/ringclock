#include <netdb.h>
#include <sdkconfig.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include "Clock.h"
#include "CountdownTimer.h"
#include "WiFi.h"
#include "cJSON.h"
#include "coap.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_task_wdt.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "lwip/apps/sntp.h"
#include "ws2812.h"

#define TIMEZONE "UTC-2"

const char *wifi_ssid = CONFIG_ESP_WIFI_SSID;
const char *wifi_psk_key = CONFIG_ESP_WIFI_PASSWORD;

enum Clock_mode { MODE_CLOCK, MODE_TIMER };
Clock_mode clock_mode = MODE_CLOCK;

WS2812 ws = WS2812(GPIO_NUM_23, 60);
Clock rclock = Clock(ws);
CountdownTimer timer = CountdownTimer(ws, 30);

WiFi wifi;

void vClockTask(void *pvParameters) {
  ESP_LOGI("vClocktask", "Starting");

  setenv("TZ", TIMEZONE, 1);
  tzset();
  while (true) {
    switch (clock_mode) {
      case MODE_CLOCK: {
        rclock.update();
      } break;
      case MODE_TIMER: {
        timer.update();
        ws.show();
        vTaskDelay(250 / portTICK_PERIOD_MS);
        if (!timer.isRunning()) {
          // sleep another 10 secs
          vTaskDelay(10000 / portTICK_PERIOD_MS);
          timer.start();
        }
      } break;
    }
  }
}
static void initialize_sntp(void) {
  ESP_LOGI("sntp", "Initializing SNTP");
  sntp_setoperatingmode(SNTP_OPMODE_POLL);
  sntp_setservername(0, "pool.ntp.org");
  sntp_init();
}

int resolve_address(const char *service, coap_address_t *dst) {
  struct addrinfo *res, *ainfo;
  struct addrinfo hints;

  memset(&hints, 0, sizeof(hints));
  memset(dst, 0, sizeof(*dst));
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_family = AF_UNSPEC;
  hints.ai_flags = AI_PASSIVE;

  if (0 != getaddrinfo(NULL, service, &hints, &res)) {
    return ESP_FAIL;
  }

  for (ainfo = res; ainfo != NULL; ainfo = ainfo->ai_next) {
    switch (ainfo->ai_family) {
      case AF_INET6:
      case AF_INET:
        dst->size = ainfo->ai_addrlen;
        memcpy(&dst->addr.sin6, ainfo->ai_addr, dst->size);
        goto finish;
      default:;
    }
  }

finish:
  freeaddrinfo(res);
  return ESP_OK;
}

void hnd_color_get(coap_context_t *ctx, struct coap_resource_t *resource,
                   coap_session_t *session, coap_pdu_t *request,
                   coap_binary_t *token, coap_string_t *query,
                   coap_pdu_t *response) {
  response->code = COAP_RESPONSE_CODE(205);
  coap_add_data(response, 3, (const uint8_t *)"get");
}

void hnd_color_put(coap_context_t *ctx, struct coap_resource_t *resource,
                   coap_session_t *session, coap_pdu_t *request,
                   coap_binary_t *token, coap_string_t *query,
                   coap_pdu_t *response) {
  size_t size;
  unsigned char *data;
  // coap_str_const_t *uri_path;
  // uri_path = coap_resource_get_uri_path(resource);
  // ESP_LOGI("CoAP", "Put path:%.*s", uri_path->length, uri_path->s);
  coap_get_data(request, &size, &data);
  // Assume its JSON
  cJSON *root = cJSON_Parse((const char *)data);
  if (root != NULL) {
    cJSON *jitem = cJSON_GetObjectItem(root, "index");
    if (jitem == NULL) goto jsonfail;
    int index = jitem->valueint;
    jitem = cJSON_GetObjectItem(root, "red");
    if (jitem == NULL) goto jsonfail;
    uint8_t red = jitem->valueint;
    jitem = cJSON_GetObjectItem(root, "green");
    if (jitem == NULL) goto jsonfail;
    uint8_t green = jitem->valueint;
    jitem = cJSON_GetObjectItem(root, "blue");
    if (jitem == NULL) goto jsonfail;
    uint8_t blue = jitem->valueint;
    rclock.set_color((clock_color_t)index, red, green, blue);
    response->code = COAP_RESPONSE_CODE(204);
    char *rendered = cJSON_Print(root);
    coap_add_data(response, strlen(rendered), (const uint8_t *)rendered);
    cJSON_Delete(root);
    return;
  }
jsonfail:
  cJSON_Delete(root);
  response->code = COAP_RESPONSE_CODE(400);
}

static coap_context_t *ctx;

void initialize_coap(void) {
  coap_endpoint_t *endpoint = nullptr;
  coap_resource_t *resource = nullptr;
  coap_address_t dst;
  coap_str_const_t ruri0 = {13, (const uint8_t *)"clock/color/0"};
  coap_str_const_t ruri1 = {13, (const uint8_t *)"clock/color/1"};
  coap_str_const_t ruri2 = {13, (const uint8_t *)"clock/color/2"};
  coap_str_const_t ruri3 = {13, (const uint8_t *)"clock/color/3"};
  coap_str_const_t ruri4 = {13, (const uint8_t *)"clock/color/4"};
  coap_str_const_t ruri5 = {13, (const uint8_t *)"clock/color/5"};
  coap_startup();
  coap_set_log_level(LOG_DEBUG);
  ESP_ERROR_CHECK(resolve_address("5683", &dst));

  ctx = coap_new_context(nullptr);
  if (!ctx || !(endpoint = coap_new_endpoint(ctx, &dst, COAP_PROTO_UDP))) {
    ESP_LOGE("Ringclock", "Cannot initialize CoAP context");
  }
  resource = coap_resource_init(&ruri0, 0);
  coap_register_handler(resource, COAP_REQUEST_GET, hnd_color_get);
  coap_register_handler(resource, COAP_REQUEST_PUT, hnd_color_put);
  coap_add_resource(ctx, resource);
  resource = coap_resource_init(&ruri1, 0);
  coap_register_handler(resource, COAP_REQUEST_GET, hnd_color_get);
  coap_register_handler(resource, COAP_REQUEST_PUT, hnd_color_put);
  coap_add_resource(ctx, resource);
  resource = coap_resource_init(&ruri2, 0);
  coap_register_handler(resource, COAP_REQUEST_GET, hnd_color_get);
  coap_register_handler(resource, COAP_REQUEST_PUT, hnd_color_put);
  coap_add_resource(ctx, resource);
  resource = coap_resource_init(&ruri3, 0);
  coap_register_handler(resource, COAP_REQUEST_GET, hnd_color_get);
  coap_register_handler(resource, COAP_REQUEST_PUT, hnd_color_put);
  coap_add_resource(ctx, resource);
  resource = coap_resource_init(&ruri4, 0);
  coap_register_handler(resource, COAP_REQUEST_GET, hnd_color_get);
  coap_register_handler(resource, COAP_REQUEST_PUT, hnd_color_put);
  coap_add_resource(ctx, resource);
  resource = coap_resource_init(&ruri5, 0);
  coap_register_handler(resource, COAP_REQUEST_GET, hnd_color_get);
  coap_register_handler(resource, COAP_REQUEST_PUT, hnd_color_put);
  coap_add_resource(ctx, resource);
}

void vTimeTask(void *pvParameters) {
  time_t now;
  struct tm timeinfo;
  char strftime_buf[64];

  rclock.set_state(false, false);
  wifi.connectAP(wifi_ssid, wifi_psk_key);
  while (wifi.isConnectedToAP() != ESP_OK) {
    ESP_LOGI("timetask", "Waiting for connection");
    vTaskDelay(2000 / portTICK_PERIOD_MS);
  }
  rclock.set_state(true, false);
  initialize_sntp();
  initialize_coap();
  while (true) {
    time(&now);
    if (now > 100000) {
      rclock.set_state(true, true);
    }

    setenv("TZ", TIMEZONE, 1);
    tzset();
    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI("timetask", "The current date/time is: %s", strftime_buf);

    coap_run_once(ctx, 10000);

    // vTaskDelay(60000 / portTICK_PERIOD_MS);
    if (wifi.isConnectedToAP() != ESP_OK) {
      rclock.set_state(false, false);
      ESP_LOGI("timetask", "Reconnecting to Wifi...");
      wifi.connectAP(wifi_ssid, wifi_psk_key);
      rclock.set_state(true, false);
    }
  }
}

extern "C" void app_main() {
  esp_log_level_set("*", ESP_LOG_INFO);
  ESP_LOGI("Ringclock", "Starting");
  // clock_mode = MODE_TIMER;
  xTaskCreate(vClockTask, "LED Task", 8000, NULL, 1, NULL);
  ESP_LOGI("Ringclock", "Task Created");

  xTaskCreate(vTimeTask, "Time Task", 8000, NULL, 1, NULL);
  ESP_LOGI("Ringclock", "Time Task Created");
  // Set up coap

  while (true) {
    esp_task_wdt_reset();
    vTaskDelay(500 / portTICK_PERIOD_MS);
  }
}
