#pragma once
#include <stdint.h>
#include <time.h>
#include "esp_log.h"
#include "ws2812.h"

class CountdownTimer {
 public:
  CountdownTimer(WS2812 &ws, uint16_t seconds) : ws(ws), seconds(seconds) {
    end_time = time(NULL) + seconds;
    running = false;
    color_left = { red: 0, green: 40, blue : 0 };
    color_done = { red: 40, green: 0, blue : 0 };
  };
  ~CountdownTimer();

  void start() { end_time = time(NULL) + seconds; running = true; };
  void stop() { running = false; };
  bool isRunning() { return running; };
  int update();
  uint16_t secondsLeft();
  void setColors(pixel_t lft, pixel_t done) { color_left = lft; color_done = done;};

 private:
  WS2812 &ws;
  uint16_t seconds;
  time_t end_time;
  bool running;
  pixel_t color_left;
  pixel_t color_done;
};