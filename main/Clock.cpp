#include "Clock.h"
#include <sdkconfig.h>
#include <sys/time.h>
#include <time.h>

void Clock::draw_face(int hour, int minute) {
  if (!wifi_up) {
    for (auto i = 0; i < 60; ++i) {
      ws.setPixel(i, colors[NOCONN]);
    }
  }
  for (auto i = 0; i < 60; i += 5) {
    ws.setPixel(i, colors[FACE]);
  }
  ws.setPixel(hour * 5 % 60 + minute / 12, colors[HOUR]);
  ws.setPixel(minute, colors[MINUTE]);
}

Clock::Clock(WS2812 &ws) : ws(ws), wifi_up(false), clock_sync(false) {
  set_color(HOUR, CONFIG_COLOR_HR_R, CONFIG_COLOR_HR_G, CONFIG_COLOR_HR_B);
  set_color(MINUTE, CONFIG_COLOR_MIN_R, CONFIG_COLOR_MIN_G, CONFIG_COLOR_MIN_B);
  set_color(SECOND, CONFIG_COLOR_SEC_R, CONFIG_COLOR_SEC_G, CONFIG_COLOR_SEC_B);
  set_color(NOCONN, CONFIG_COLOR_NC_R, CONFIG_COLOR_NC_G, CONFIG_COLOR_NC_B);
  set_color(NOSYNC, CONFIG_COLOR_SNTP_R, CONFIG_COLOR_SNTP_G,
            CONFIG_COLOR_SNTP_B);
  set_color(FACE, CONFIG_COLOR_FACE_R, CONFIG_COLOR_FACE_G,
            CONFIG_COLOR_FACE_B);
}
pixel_t Clock::get_color(clock_color_t c) { return colors[c]; }

void Clock::set_color(clock_color_t c, uint8_t r, uint8_t g, uint8_t b) {
  if (c < COLORCOUNT) {
    colors[c].red = r;
    colors[c].green = g;
    colors[c].blue = b;
  }
}
void Clock::set_color(clock_color_t c, pixel_t col) {
  if (c < COLORCOUNT) colors[c] = col;
}

void Clock::update() {
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = 10 / portTICK_PERIOD_MS;

  time_t now = 0;
  struct tm timeinfo = {.tm_sec = 0,
                        .tm_min = 0,
                        .tm_hour = 0,
                        .tm_mday = 0,
                        .tm_mon = 0,
                        .tm_year = 0,
                        .tm_wday = 0,
                        .tm_yday = 0,
                        .tm_isdst = 0};
  time(&now);
  localtime_r(&now, &timeinfo);
  xLastWakeTime = xTaskGetTickCount();

  for (auto j = 0; j < 100; j++) {
    ws.clear();
    draw_face(timeinfo.tm_hour % 12, timeinfo.tm_min);
    pixel_t cur = ws.getPixel(timeinfo.tm_sec);
    pixel_t nxt = ws.getPixel((timeinfo.tm_sec + 1) % 60);
    ws.setPixel(timeinfo.tm_sec, ws.interpolate(colors[SECOND], cur, j));
    ws.setPixel((timeinfo.tm_sec + 1) % 60,
                ws.interpolate(nxt, colors[SECOND], j));
    ws.show();
    vTaskDelayUntil(&xLastWakeTime, xFrequency);
  }
}