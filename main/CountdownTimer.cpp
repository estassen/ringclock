#include <sdkconfig.h>
#include "esp_log.h"
#include "CountdownTimer.h"

CountdownTimer::~CountdownTimer() {}

int CountdownTimer::update() {
  if (running) {
    clock_t now = time(NULL);
    ESP_LOGI("Timer", "Now: %u End: %i", (unsigned int)now, (unsigned int)end_time );
    if (end_time > now) {
      int pixels_left = (ws.getPixelCount() * (end_time - now)) / seconds;
      for (auto i = 0; i < ws.getPixelCount(); i++) {
        if (i < pixels_left) {
          ws.setPixel(i, color_left);
        } else {
          ws.setPixel(i, color_done);
        }
      }
      return 1;
    } else {
      running = false;
    }
  }
  return 0;
}
