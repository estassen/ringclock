#pragma once
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "ws2812.h"

typedef enum CLOCK_COLOR {HOUR=0, MINUTE, SECOND, NOCONN, NOSYNC, FACE, COLORCOUNT} clock_color_t;

class Clock {
 public:
  Clock(WS2812 &ws);
  void update();
  void set_state(bool wifi, bool sntp) {
    wifi_up = wifi;
    clock_sync = sntp;
  };
  pixel_t get_color(clock_color_t c);
  void set_color(clock_color_t c, uint8_t r, uint8_t g, uint8_t b);
  void set_color(clock_color_t c, pixel_t col);

 private:
  WS2812 &ws;
  bool wifi_up;
  bool clock_sync;
  pixel_t colors[COLORCOUNT];
  void draw_face(int hour, int minute);
};
